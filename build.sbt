lazy val akkaHttpVersion = "10.1.11"
lazy val akkaVersion    = "2.6.3"

assemblyJarName in assembly := "apiserver.jar"
mainClass in assembly := Some("ExerciseApiApp")

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.example",
      scalaVersion    := "2.12.8"
    )),
    name := "api-exercise-app",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"   % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % "2.5.26",
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "ch.qos.logback"    % "logback-classic"           % "1.2.3",

      "com.nrinaudo" %% "kantan.csv" % "0.5.1",
      "com.nrinaudo" %% "kantan.csv-generic" % "0.5.1",

      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.1",
      "org.postgresql" % "postgresql" % "42.2.6",
      "com.h2database" % "h2" % "1.4.187",

      "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.26",
      "com.typesafe.akka" %% "akka-http-testkit"        % akkaHttpVersion % Test,
      "org.scalatest"     %% "scalatest"                % "3.0.8"         % Test
    ),
    dockerBaseImage := "openjdk:8-jre-alpine"
  ).enablePlugins(JavaAppPackaging, DockerPlugin, AshScriptPlugin)
