# API Exercise README

## Introduction
This project consists in creating an HTTP-API (e.g. REST) that allows reading, writing, updating and deleting data from a database.
The API documentation can be found under `instructions/API.md`, and the OpenAPI specification is provided under `instructions/swagger.yml`. 

## Application
This project was initialized using a template from Lightbend Project Starter [link](https://developer.lightbend.com/guides/akka-http-quickstart-scala/index.html).

The application was developed using the Scala language with Akka Http, and using SBT as the build tool ([sbt](https://www.scala-sbt.org/index.html)).
The database chosen was PostgreSQL.

`ExerciseApiApp` contains the main application, which performs the following operations:
1. Reads the data from the csv file;
2. Creates the database table to be used (if it does not exist);
3. Writes the data to the database;
4. Starts the main web server application which runs the routes for the API.

### How to run
In order to be able to run this project locally you need to install sbt by following the instructions in [sbt docs](https://www.scala-sbt.org/1.x/docs/Setup.html).

In order to be able to run the application, you need to have a database instance running, and adjust the configuration settings in `resources/application.conf` accordingly,
namely the jdbc connection string under setting `postgres.url`. You will also need to set the environment variables POSTGRES_USER, POSTGRES_PASSWORD and POSTGRES_DB.
You can run the application locally using:
```
sbt run
```
However, the recommended way to run this application is by using `docker-compose` (see instructions below). 
A script is also provided for this purpose: `init-apiserver.sh`.

### Possible improvements
- **Writing tests**:

Tests are not provided due to not having enough time to write them, and the application was only tested manually and in a more or less ad-hoc way using some `curl` requests.

DISCLAIMER: I would never write an application that runs in production without ensuring proper test coverage and would consider
this as an essential part in the development process.

- **Split main application in 2 stages**:

We should have different applications in the same build: one for loading the dataset initially provided to the database (which is meant to be a one time procedure),
and another for running the routes. The 2 parts should actually be run independently. Note that currently if we run the application
several times, the data will be fed into the database, and a new UUID will be generated for each record, which is probably not what we would like to have.

An alternative to this would be to add a command line argument that would allow determining if we should fill in the database 
with data in a specified file location, or simply to start the server. However, the former solution would be preferable to this.

- **Better types**:

As an example, UUIDs should be proper Java UUIDs, and not Strings. This could also be beneficial for data field validation purposes.

## Docker
A setup using docker-compose is provided. In order to build / tear down, the following commands can be used:
```
docker-compose up --build
docker-compose down --remove-orphans --volumes 
```

### Building the image
The docker image for the Postgres database is pulled from docker hub. You can build the docker image for the API server in one of 2 ways:
1. Using the provided `Dockerfile` and running `docker-compose` as it is. 
This uses a multi-stage docker build with 2 stages: in the first one the jar is built (using the plugin from sbt [sbt-assembly](https://github.com/sbt/sbt-assembly), and
in the second one a lightweight docker image with JDK is pulled and the jar from the previous stage is used to run the application.

2. With sbt native packager plugin (note that in order to do this you need to have your local environment setup with sbt, Scala, JDK/JRE), by running:
```
sbt docker:publishLocal 
```
The resulting generated docker file can then be found under `target/docker/stage/Dockerfile`.
After that, you need to replace in the docker-compose file the following lines:
```
apiserver:
    build: .
```
with:
```
apiserver:
    image: api-exercise-app:0.1.0-SNAPSHOT
```
and then running the `docker-compose up` command. 

Note that the Dockerfile generated in method 2 will be different from the one provided and used in method 1.

### Implementation notes
- For passing the credentials to the database, we are using environment variables stored in the `.env` file (see [docker docs](https://docs.docker.com/compose/environment-variables/#/the-env-file#the-env-file), and referencing 
this in docker-compose. According to best practices, this file should be added to `.gitignore`, and stored either locally or 
at a secure location that can be shared with a team, but for the sake of the current exercise this procedure was not followed.
- We use a volume named `titanic_dbdata` for storing the database data, which guarantees persistence even if the container and image are deleted.
This volume will remain unless it is explicitly deleted using `docker volume rm`.

### Possible improvements
- **Adding caching**:

SBT uses ivy to resolve project dependencies (located in `~/.ivy2`), which caches the downloaded artifacts locally, so every time it is asked to pull them it goes to that cache first
and it only downloads from remote if nothing is found there. One way to enable caching would be to map both `~/.ivy2` and `~/.sbt` to volumes in a docker container, adding the entry:
```
    volumes:
      - ~/.ivy2:<some-path>/.ivy2
      - ~/.sbt:<some-path>/.sbt
```
- **Use SBT Native Packager instead of providing our own Dockerfile**:

We could have performed all of the configurations for docker inside `build.sbt`, using the functionality enabled by the 
docker plugin in [SBT Native Packager](https://www.scala-sbt.org/sbt-native-packager/formats/docker.html).

## Kubernetes
For deploying to Kubernetes, a setup using [Minikube](https://minikube.sigs.k8s.io/) was used. 
In order to start Minikube you should run:
```
minikube start --memory 8192
```
Then you need to run:
```
eval $(minikube docker-env)
```

The following sequence of commands is used to setup the kubernetes deployment:
```
docker build ./ -t apiserver

kubectl create -f k8s/db-secrets.yml
kubectl create -f k8s/db-volume.yml
kubectl create -f k8s/db-deployment.yml
kubectl create -f k8s/db-service.yml
kubectl create -f k8s/apiserver-deployment.yml
kubectl create -f k8s/apiserver-service.yml
```

You can try accessing the running server using, for example (after waiting for the API server to be completely started):
```
curl -v http://$(minikube ip):31317/people
```

There are separate deployments for the database and the API webserver, including the following assets:
1. Postgres Database:
- Secrets for the database are set up in `k8s/db-secrets.yml`;
- The configuration for a volume and respective volume claim are done in `k8s/db-secrets.yml`;
- The deployment is configured in `k8s/db-deployment.yml`;
- The service definition is found in `k8s/db-service.yml`.
2. API server:
- The deployment is configured in `k8s/apiserver-deployment.yml` and uses the database secrets defined above;
- The service definition is found in `k8s/apiserver-service.yml`.

### Possible improvements
- A script should be added for running all the steps in the Kubernetes deployment, making sure that the DB is setup and running before starting the application server.
