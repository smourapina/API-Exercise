import akka.http.scaladsl.Http
import domain.Person
import http.PeopleRoutes
import slick.jdbc.PostgresProfile.api._
import persistence.{CsvParser, PersonRepository}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

object ExerciseApiApp extends App {

  implicit val system = ActorSystem("api-exercise-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  println("Starting...")

  val csvParser = new CsvParser()
  val registry: Seq[Person] = csvParser.readSource("titanic.csv")

  val db = Database.forConfig("postgres")
  val repo = new PersonRepository(db)

  Await.result(repo.createIfNotExists(), Duration.Inf)
  registry.foreach { person =>
    Await.result(repo.insertPassenger(person), Duration.Inf)
  }
  val getPassengers = Await.result(repo.retrievePassengers(), Duration.Inf)

  val routes = new PeopleRoutes(repo)(system)

  Http().bindAndHandle(routes.peopleRoutes, "0.0.0.0", 8080)

  println(s"Server online at http://localhost:8080/")

}
