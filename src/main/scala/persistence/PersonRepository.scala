package persistence

import domain.Person
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.meta.MTable
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

class PersonRepository(db: Database) {

  val titanicTable = TableQuery[Passengers]

  val tables = List(titanicTable)

  def retrievePassengers(): Future[Seq[Person]] = {
    db.run(titanicTable.result)
  }

  def insertPassenger(passenger: Person): Future[Int] = {
    db.run(titanicTable.insertOrUpdate(passenger))
  }

  def retrievePassenger(uuid: String): Future[Option[Person]] = {
    db.run(titanicTable.filter(_.uuid === uuid).result.headOption)
  }

  def updatePassenger(passenger: Person): Future[Int] = {
    db.run(titanicTable.insertOrUpdate(passenger))
  }

  def deletePassenger(uuid: String): Future[Int] = {
    db.run(titanicTable.filter(_.uuid === uuid).delete)
  }

  def createIfNotExists() = {
    val existing = db.run(MTable.getTables)
    existing.flatMap( v => {
      val names = v.map(mt => mt.name.name)
      val createIfNotExist = Seq(titanicTable).filter( table =>
        (!names.contains(table.baseTableRow.tableName))).map(_.schema.create)
      db.run(DBIO.sequence(createIfNotExist))
    })
  }

}
