package persistence

import java.util.UUID

import domain.Person
import slick.jdbc.H2Profile.api._

class Passengers(tag: Tag) extends Table[Person](tag, "titanic") {
  def uuid = column[String]("UUID", O.PrimaryKey)
  def survived = column[Boolean]("SURVIVED")
  def passengerClass = column[Int]("CLASS")
  def name = column[String]("NAME")
  def sex = column[String]("SEX")
  def age = column[Int]("AGE")
  def siblingsSpouses = column[Int]("SIBLINGS_SPOUSES")
  def parentsChildren = column[Int]("PARENTS_CHILDREN")
  def fare = column[Float]("FARE")

  def * = (uuid, name, survived, passengerClass, sex, age, siblingsSpouses, parentsChildren, fare) <> (Person.tupled, Person.unapply)
}
