package persistence

import java.util.UUID

import domain.{Person, PersonDataCsv, Conversions}

import scala.language.higherKinds
import kantan.csv.{CsvReader, ReadResult, rfc}
import kantan.csv.ops._
import kantan.csv.generic._

import scala.io.Source

class CsvParser {
  private val passengerRegistry: scala.collection.mutable.Set[Person] =
    scala.collection.mutable.Set.empty[Person]

  def readSource(filename: String): Seq[Person] = {
    val sourceCsvFile: String = Source.fromResource(filename).mkString

    val reader: CsvReader[ReadResult[PersonDataCsv]] =
      sourceCsvFile.asCsvReader[PersonDataCsv](rfc.withHeader)

    reader.foreach { data =>
      if (data.isRight)
        data.map { v =>
          passengerRegistry.add(
            Conversions.personDataToPerson(v))
        }
    }
    passengerRegistry.toSeq
  }

  def retrievePassengerRegistry: List[Person] = passengerRegistry.toList
}
