package http

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import persistence.PersonRepository
import domain.{Conversions, People, Person, PersonData}

import scala.concurrent.Future

class PeopleRoutes(personRepository: PersonRepository)(implicit val system: ActorSystem) {

  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  import domain.JsonFormats._
  implicit val executionContext = system.dispatcher

  private implicit val timeout = Timeout.create(system.settings.config.getDuration("my-app.routes.ask-timeout"))

  def getPeople(): Future[People] = personRepository.retrievePassengers().map(People(_))
  def createPerson(person: Person): Future[Int] = personRepository.insertPassenger(person)

  def getPerson(uuid: String): Future[Option[Person]] = personRepository.retrievePassenger(uuid)
  def putPerson(uuid: String, person: PersonData): Future[Int] = {
    val toUpdate: Future[Option[Person]] = getPerson(uuid)
    toUpdate.flatMap {
      case Some(_) =>
        val updatedData = Conversions.personDataToNewPerson(Some(uuid), person)
        personRepository.updatePassenger(updatedData)
      case None => Future.successful(0)
    }
  }
  def deletePerson(uuid: String): Future[Int] = personRepository.deletePassenger(uuid)

  val peopleRoutes: Route =
    pathPrefix("people") {
      concat(
        pathEnd {
          concat(
            get {
              onSuccess(getPeople()) {
                p => complete((StatusCodes.OK, p))
              }
            },
            post {
              entity(as[PersonData]) { person =>
                val newPerson = Conversions.personDataToNewPerson(None, person)
                onSuccess(createPerson(newPerson)) { _ =>
                  complete((StatusCodes.Created, newPerson))
                }
              }
            })
        },
        path(Segment) { uuid =>
          concat(
            get {
              rejectEmptyResponse {
                onSuccess(getPerson(uuid)) { response =>
                  complete(response)
                }
              }
            },
            put {
              entity(as[PersonData]) { person =>
                onSuccess(putPerson(uuid, person)) { _ =>
                  complete(StatusCodes.OK)
                }
              }
            },
            delete {
              onSuccess(deletePerson(uuid)) { _ =>
                complete(StatusCodes.OK)
              }
            })
        }
      )
    }
}
