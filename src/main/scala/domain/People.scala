package domain

final case class Person(uuid: String,
                        name: String,
                        survived: Boolean,
                        passengerClass: Int,
                        sex: String,
                        age: Int,
                        siblingsOrSpousesAboard: Int,
                        parentsOrChildrenAboard: Int,
                        fare: Float)
final case class People(people: Seq[Person])

final case class PersonData(survived: Boolean,
                            passengerClass: Int,
                            name: String,
                            sex: String,
                            age: Int,
                            siblingsOrSpousesAboard: Int,
                            parentsOrChildrenAboard: Int,
                            fare: Float)

final case class PersonDataCsv(survived: Int,
                            passengerClass: Int,
                            name: String,
                            sex: String,
                            age: Int,
                            siblingsSpouses: Int,
                            parentsChildren: Int,
                            fare: Double)
