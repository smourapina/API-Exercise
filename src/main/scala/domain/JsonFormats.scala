package domain

import spray.json.DefaultJsonProtocol

object JsonFormats  {
  import DefaultJsonProtocol._

  implicit val personJsonFormat = jsonFormat9(Person)
  implicit val peopleJsonFormat = jsonFormat1(People)
  implicit val personDataJsonFormat = jsonFormat8(PersonData)

}
