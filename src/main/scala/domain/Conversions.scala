package domain

import java.util.UUID

object Conversions {

  def survivedToBoolean(survived: Int): Boolean = if (survived == 1) true else false

  def personDataToPerson(dataRow: PersonDataCsv): Person = {
    Person(
      uuid = UUID.randomUUID().toString,
      survived = survivedToBoolean(dataRow.survived),
      passengerClass = dataRow.passengerClass,
      name = dataRow.name,
      sex = dataRow.sex,
      age = dataRow.age,
      siblingsOrSpousesAboard = dataRow.siblingsSpouses,
      parentsOrChildrenAboard = dataRow.parentsChildren,
      fare = dataRow.fare.toFloat
    )
  }

  def personDataToNewPerson(uuid: Option[String], personData: PersonData): Person = {
    Person(
      uuid = uuid.getOrElse(UUID.randomUUID().toString),
      survived = personData.survived,
      passengerClass = personData.passengerClass,
      name = personData.name,
      sex = personData.sex,
      age = personData.age,
      siblingsOrSpousesAboard = personData.siblingsOrSpousesAboard,
      parentsOrChildrenAboard = personData.parentsOrChildrenAboard,
      fare = personData.fare
    )
  }

}