FROM bigtruedata/sbt AS build
WORKDIR /root/app
COPY . .
RUN sbt assembly

FROM openjdk:8-alpine
LABEL maintainer "Silvia Pina <silviampina@gmail.com>"
RUN apk update && apk upgrade
RUN addgroup -g 9000 -S apigroup && \
  adduser -S -G apigroup apiuser
USER apiuser
COPY --from=build /root/app/target/scala-2.12/apiserver.jar .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "apiserver.jar"]